import { Controller } from "@hotwired/stimulus";
import AOS from "aos";
import "aos/dist/aos.css";
export default class extends Controller {
  connect() {
    // Cursor custom

    const pointer = document.querySelector(".pointer");
    document.addEventListener("mousemove", function (event) {
      pointer.style.left = event.pageX + "px";
      pointer.style.top = event.pageY + "px";
    });

    //  Animation on scroll (AOS)

    AOS.init({
      once: true,
    });

    // Init lightbox

    const { initLightbox } = require("../controllers/lightbox_controller");
    initLightbox();

    // require("select2");
    // require("select2/dist/css/select2.min.css");
    // $(function () {
    //   $(".select2-custom").select2();
    // });

    // Moment JS countdown

    const { initCountdownTimer } = require("../controllers/moment_controller");
    initCountdownTimer();
  }
}
