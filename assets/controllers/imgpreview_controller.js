import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    const smallPreviews = document.querySelectorAll(".secondary-img");
    const bigPreviews = document.querySelector(".item-img");

    smallPreviews.forEach((element) => {
      element.addEventListener("click", function (e) {
        e.preventDefault();
        bigPreviews.src = element.src;
      });
    });
  }
}
