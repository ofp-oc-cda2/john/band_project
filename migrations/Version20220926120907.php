<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220926120907 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gallery (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gallery_user (gallery_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_1E8CFEC54E7AF8F (gallery_id), INDEX IDX_1E8CFEC5A76ED395 (user_id), PRIMARY KEY(gallery_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gallery_user ADD CONSTRAINT FK_1E8CFEC54E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gallery_user ADD CONSTRAINT FK_1E8CFEC5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gallery_user DROP FOREIGN KEY FK_1E8CFEC54E7AF8F');
        $this->addSql('ALTER TABLE gallery_user DROP FOREIGN KEY FK_1E8CFEC5A76ED395');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('DROP TABLE gallery_user');
    }
}
