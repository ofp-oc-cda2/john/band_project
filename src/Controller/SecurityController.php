<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use ApiPlatform\Core\Api\IriConverterInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


    
    class SecurityController extends AbstractController
{
    #[Route('/login', name: 'app_login')]
     public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'last_username' => $authenticationUtils->getLastUsername(),
        ]);
    }


    #[Route('/logout', name: 'app_logout')]
    public function logout()
    {
        throw new \Exception('logout() should never be reached');
    }
}
    // #[Route('/login', name: 'app_login')]
    // public function login(): Response
    // {
    //     if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
    //         return $this->json([
    //             'error' => 'Invalid login request: check that the Content-Type header is "application/json".'
    //         ], 400);
    //  }
    //       return new Response(null, 204, [
    //         'Location' => $iriConverter->getIriFromItem($this->getUser())
    //     ]);
    // }

    // #[Route('/logout', name: 'app_logout')]
    // public function logout(): void
    // {
    //     throw new \Exception('This should never be reached!');
    // }

