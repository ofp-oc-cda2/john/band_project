import { Controller } from "@hotwired/stimulus";
const { initLightbox } = require("../controllers/lightbox_controller");
export default class extends Controller {
  connect() {
    const seeMoreGalleryButton = document.querySelector(".see-more-gallery");
    let page = parseInt(seeMoreGalleryButton.getAttribute("data-page"));
    seeMoreGalleryButton.addEventListener("click", function () {
      getPaginatedGallery(page, seeMoreGalleryButton);
    });
    

    function getPaginatedGallery(page, seeMoreGalleryButton) {
      $.ajax({
        url: `/gallery/pagination?page=${page}`,
        type: "GET",
        success: function (response) {
          seeMoreGalleryButton.setAttribute("data-page", page + 1);
          document.querySelector(".gallery-section").innerHTML += response.gallery;
          if (page * parseInt(response.limit) >= parseInt(response.totalGallery)) {
            seeMoreGalleryButton.style.display = "none";
          }
          initLightbox();
        },
      });
    }
  }
}