<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220928083818 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gallery ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783AB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783A896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_472B783AB03A8386 ON gallery (created_by_id)');
        $this->addSql('CREATE INDEX IDX_472B783A896DBBDE ON gallery (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE gallery DROP FOREIGN KEY FK_472B783AB03A8386');
        $this->addSql('ALTER TABLE gallery DROP FOREIGN KEY FK_472B783A896DBBDE');
        $this->addSql('DROP INDEX IDX_472B783AB03A8386 ON gallery');
        $this->addSql('DROP INDEX IDX_472B783A896DBBDE ON gallery');
        $this->addSql('ALTER TABLE gallery DROP created_by_id, DROP updated_by_id, DROP created_at, DROP updated_at');
    }
}
