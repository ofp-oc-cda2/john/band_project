import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    $(document).ready(function () {
      const imageLink = $("#product").data("src");
      $("#product").zoom({
        url: imageLink,
        magnify: 2,
      });
    });
  }
}
