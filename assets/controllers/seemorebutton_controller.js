import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  connect() {
    console.log("seemorebutton_controller connected");
    const prototype = document.querySelector(".shop-section").innerHTML;
    document
      .getElementById("see-more-button")
      .addEventListener("click", function (e) {
        e.preventDefault();
        console.log("button click");
        document.querySelector(".shop-section").innerHTML += prototype;
      });
  }
}
