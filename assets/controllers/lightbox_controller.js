import Lightbox from "bs5-lightbox";

 // Lightbox for bs5

export const initLightbox = function () {
  const options = {
    keyboard: true,
    size: "fullscreen",
  };

  document.querySelectorAll(".my-lightbox-toggle").forEach((el) =>
    el.addEventListener("click", (e) => {
      e.preventDefault();
      const lightbox = new Lightbox(el, options);
      lightbox.show();
    })
  );
};
