<?php

namespace App\Controller\Admin;

use App\Entity\NextDate;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class NextDateCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NextDate::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')
            ->onlyOnIndex();
        yield DateField::new('date');
    }
}
