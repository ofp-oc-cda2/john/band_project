import moment from "moment/moment";

// Lightbox for bs5

export const initCountdownTimer = () => {
  const nextDateUnix = document
    .querySelector(".countdown")
    .getAttribute("data-next-date");
  setInterval(function () {
    document.querySelectorAll(".countdown").forEach((element) => {
      element.textContent = `${durationAsString(moment(), moment.unix(nextDateUnix))}`;
    });
  });
};

const durationAsString = (start, end) => {
  const duration = moment.duration(moment(end).diff(moment(start)));

  //Get Days
  const days = Math.floor(duration.asDays()); // .asDays returns float but we are interested in full days only
  const daysFormatted = days ? `${days}d ` : ""; // if no full days then do not display it at all

  //Get Hours
  const hours = duration.hours();
  const hoursFormatted = `${hours}h `;

  //Get Minutes
  const minutes = duration.minutes();
  const minutesFormatted = `${minutes}m`;

  //Get Seconds
  const seconds = duration.seconds();
  const secondsFormatted = `${seconds}s`;

  return [
    daysFormatted,
    hoursFormatted,
    minutesFormatted,
    secondsFormatted,
  ].join(" ");
};
