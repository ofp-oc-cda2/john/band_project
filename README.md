# The Attitude Band project

![](public\images\logo.jpg)

#
#






>Creation of a website for a rock band with Symfony6
> API rest + Mobile application using Flutter



## Tech

- [Stimulus JS] - Framework JS
- [Flutter] - Framework for mobile app
- [Symfony 6] - Framework PHP
- [GitLab] - Team working and versionning
- [SCSS] - Some style
- [Docker] - Container for developing an app 

## Development

App Functionalities :

- A map with upcoming shows
- The user can ask to be notified of upcoming concerts within a defined radius and a newsletter
- Integrated music player
- Merchandising and payments
- Interface admin
- Account 
- Display social network news




## License

MIT

**Free Software, Hell Yeah!**


