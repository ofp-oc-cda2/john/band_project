<?php

namespace App\Controller;

use App\Entity\NextDate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\NextDateRepository;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function home(NextDateRepository $nextDateRepository): Response
    {
        $result = $nextDateRepository->findOneBy([],['id'=>'DESC']);
        return $this->render('home/homepage.html.twig', [
            'controller_name' => 'HomeController',
            'nextDate' => $result->getDate()
        ]);
    }
}
