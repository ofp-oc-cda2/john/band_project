import { Controller } from "@hotwired/stimulus";

// Le contrôleur hérite de la classe `Controller` de Stimulus
export default class extends Controller {

  // La méthode `connect()` est appelée lorsque le contrôleur est connecté à la page
  connect() {

    // On récupère une référence au bouton "see-more" en utilisant `document.querySelector()`
    const seeMoreNewsButton = document.querySelector(".see-more-news");

    // On extrait le numéro de page à partir de l'attribut "data-page" du bouton
    let page = parseInt(seeMoreNewsButton.getAttribute("data-page"));

    // On ajoute un écouteur d'événement pour le clic sur le bouton "see-more"
    seeMoreNewsButton.addEventListener("click", function () {
      getPaginatedNews(page, seeMoreNewsButton);
    });

    // Cette fonction est appelée lorsque le bouton "see-more" est cliqué
    function getPaginatedNews(page, seeMoreNewsButton) {

      // On utilise jQuery pour envoyer une requête AJAX et obtenir le contenu des actualités dans la base de données
      $.ajax({
        url: `/news/pagination?page=${page}`,
        type: "GET",
        success: function (response) {

          // On met à jour l'attribut "data-page" du bouton pour la page suivante
          seeMoreNewsButton.setAttribute("data-page", page + 1);

          // On ajoute le contenu des actualités à la section "news-section" de la page
          document.querySelector(".news-section").innerHTML += response.news;

          // On masque le bouton "see-more" s'il n'y a plus d'actualités à charger
          if (page * parseInt(response.limit) >= parseInt(response.totalNews)) {
            seeMoreNewsButton.style.display = "none";
          }
        },
      });
    }
  }
}