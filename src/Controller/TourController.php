<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ConcertRepository;

class TourController extends AbstractController
{
   #[Route('/tour', name: 'app_tour')]
    public function showTour(ConcertRepository $concertsRepo): Response
    {
        $allDates = $concertsRepo->findAll();
        return $this->render('tour/tourpage.html.twig', [
            'controller_name' => 'TourController',
            'allDates' => $allDates
        ]);
    }

     #[Route('/tour/event/{id}', name: 'app_event')]
    public function showDate($id, ConcertRepository $concertsRepo): Response
    {   
        $concert = $concertsRepo->find($id);
        return $this->render('tour/event/eventpage.html.twig', [
            'controller_name' => 'TourController',
            'concert' => $concert
        ]);
    }
}