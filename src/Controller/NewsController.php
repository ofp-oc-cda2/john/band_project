<?php

namespace App\Controller;

use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\NewsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends AbstractController
{
    #[Route('/news', name: 'app_news')]
    public function showNews(NewsRepository $newsRepo): Response
    {
        $allNews = $newsRepo->getPaginatedNews(1, News::LIMIT);
        return $this->render('news/newspage.html.twig', [
            'controller_name' => 'NewsController',
            'allNews' => $allNews
        ]);
    }

    #[Route('/news/article/{id}', name: 'app_article')]
    public function showArticle($id, NewsRepository $newsRepo): Response
    {
        $news =  $newsRepo->find($id);
        return $this->render('news/article/article.html.twig', [
            'controller_name' => 'NewsController',
            'news' => $news
        ]);
    }

    #[Route('/news/pagination', name: 'app_pagination', methods: ['POST', 'GET'])]
    public function showPagination(NewsRepository $newsRepo, Request $request): JsonResponse
    {
      
        // On récupère le numéro de page
        $page = (int)$request->query->get("page", 1);

        // On récupère les annonces de la page en fonction du filtre
        $news = $newsRepo->getPaginatedNews($page, News::LIMIT);

        // On récupère le nombre total d'annonces
        $totalNews = $newsRepo->getTotalNews();

        return new JsonResponse([
            'news' => $this->renderView('news/pagination/pagination.html.twig', [
                'controller_name' => 'PaginationController',
                'allNews' => $news
            ]),
            "totalNews" => $totalNews,
            "limit" => News::LIMIT
        ]);
    }
}
