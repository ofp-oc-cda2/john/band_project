<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WelcomeController extends AbstractController
{
    #[Route('/home', name: 'app_welcome')]
    public function welcome(): Response
    {
        return $this->render('welcome/welcomepage.html.twig', [
            'controller_name' => 'WelcomeController',
        ]);
    }
}
