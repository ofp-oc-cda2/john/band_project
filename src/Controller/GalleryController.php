<?php

namespace App\Controller;

use App\Entity\Gallery;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GalleryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends AbstractController
{
    #[Route('/gallery', name: 'app_gallery')]
    public function showGallery(GalleryRepository $galleryRepo): Response
    {   
        $allGalleries = $galleryRepo->getPaginatedGallery(1, Gallery::LIMIT);
        return $this->render('gallery/gallery.html.twig', [
            'controller_name' => 'GalleryController',
            'allGalleries' => $allGalleries
        ]);
    }

    #[Route('/gallery/pagination', name: 'app_gallerypagination', methods: ['POST', 'GET'])]
    public function showPagination(GalleryRepository $galleryRepo, Request $request): JsonResponse
    {
      
        // On récupère le numéro de page
        $page = (int)$request->query->get("page", 1);

        // On récupère les annonces de la page en fonction du filtre
        $gallery = $galleryRepo->getPaginatedGallery($page, Gallery::LIMIT);

        // On récupère le nombre total d'annonces
        $totalGallery = $galleryRepo->getTotalGallery();

        return new JsonResponse([
            'gallery' => $this->renderView('gallery/pagination-gallery/pagination-gallery.html.twig', [
                'controller_name' => 'PaginationGalleryController',
                'allGalleries' => $gallery
            ]),
            "totalGallery" => $totalGallery,
            "limit" => Gallery::LIMIT
        ]);
    }
}
