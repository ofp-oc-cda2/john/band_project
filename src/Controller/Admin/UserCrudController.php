<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AvatarField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')
            ->onlyOnIndex();
        yield AvatarField::new('avatar')
            ->formatValue(static function ($value, User $user) {
                return $user->getAvatarUrl();
            })
            ->hideOnForm();
        yield ImageField::new('avatar')
            ->setBasePath('/uploads/avatars')
            ->setUploadDir('public/uploads/avatars')
            ->setUploadedFileNamePattern('[slug]-[timestamp].[extension]')
            ->onlyOnForms();
        yield EmailField::new('email');
        yield TextField::new('fullName')
            ->hideOnForm();
        yield Field::new('username')
            ->onlyOnForms();
        yield Field::new('firstName')
            ->onlyOnForms();
        yield Field::new('lastName')
            ->onlyOnForms();
        yield Field::new('password')
            ->onlyOnForms();
        yield Field::new('country')
            ->onlyOnForms();
        yield Field::new('region')
            ->onlyOnForms();
        yield Field::new('city')
            ->onlyOnForms();
        yield Field::new('adress')
            ->onlyOnForms();
        yield Field::new('postcode')
            ->onlyOnForms();
        yield DateField::new('createdAt')
            ->hideOnForm();
        yield DateField::new('UpdatedAt ')
            ->hideOnForm();
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderExpanded()
            ->renderAsBadges();
        yield BooleanField::new('enabled')
            ->renderAsSwitch(false);
        yield Field::new('isVerified')
                ->onlyOnForms();
    }
    
}
