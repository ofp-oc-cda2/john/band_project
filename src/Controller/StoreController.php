<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StoreController extends AbstractController
{
    #[Route('/store', name: 'app_store')]
    public function showStore(): Response
    {
        return $this->render('store/storepage.html.twig', [
            'controller_name' => 'StoreController',
        ]);
    }

    #[Route('/store/item', name: 'app_item')]
    public function showItem(): Response
    {
        return $this->render('store/item/item.html.twig', [
            'controller_name' => 'StoreController',
        ]);
    }
}