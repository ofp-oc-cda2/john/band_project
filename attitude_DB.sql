-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 28 oct. 2022 à 15:53
-- Version du serveur :  8.0.31-0ubuntu0.20.04.1
-- Version de PHP : 8.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `attitude_DB`
--

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE `city` (
  `id` int NOT NULL,
  `department_id` int DEFAULT NULL,
  `gps_lng` double NOT NULL,
  `gps_lat` double NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `city`
--

INSERT INTO `city` (`id`, `department_id`, `gps_lng`, `gps_lat`, `slug`, `zip_code`, `name`) VALUES
(1, 1, 0, 0, 'string', 0, 'string');

-- --------------------------------------------------------

--
-- Structure de la table `concert`
--

CREATE TABLE `concert` (
  `id` int NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `concert`
--

INSERT INTO `concert` (`id`, `description`, `title`, `date`, `location`) VALUES
(2, 'The Attitude en concert', 'Besançon, FR', '2022-09-02 00:00:00', 'Ebulisson festival'),
(3, 'Je suis une description', 'Besançon, FR', '2022-09-03 00:00:00', 'Roger center');

-- --------------------------------------------------------

--
-- Structure de la table `concert_user`
--

CREATE TABLE `concert_user` (
  `concert_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `department`
--

CREATE TABLE `department` (
  `id` int NOT NULL,
  `region_id` int DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `department`
--

INSERT INTO `department` (`id`, `region_id`, `slug`, `code`, `name`) VALUES
(1, 2, 'string', 0, 'string');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220831132827', '2022-08-31 15:28:34', 67729),
('DoctrineMigrations\\Version20220901143115', '2022-09-01 16:31:23', 13774),
('DoctrineMigrations\\Version20220902090745', '2022-09-02 11:07:49', 14636),
('DoctrineMigrations\\Version20220926120907', '2022-09-26 14:09:46', 11537),
('DoctrineMigrations\\Version20220928083818', '2022-09-28 10:38:36', 12352),
('DoctrineMigrations\\Version20221027074347', '2022-10-27 09:44:02', 10699);

-- --------------------------------------------------------

--
-- Structure de la table `gallery`
--

CREATE TABLE `gallery` (
  `id` int NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by_id` int DEFAULT NULL,
  `updated_by_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `created_by_id`, `updated_by_id`, `created_at`, `updated_at`) VALUES
(15, 'show-purple-1664354461.png', 13, 13, '2022-09-28 10:41:01', '2022-09-28 10:41:01'),
(16, 'photo-vendredi-8-1664354469.png', 13, 13, '2022-09-28 10:41:09', '2022-09-28 10:41:09'),
(17, 'photo-vendredi-9-1664354475.png', 13, 13, '2022-09-28 10:41:15', '2022-09-28 10:41:15'),
(18, 'photo-vendredi-10-1664354481.png', 13, 13, '2022-09-28 10:41:21', '2022-09-28 10:41:21'),
(19, 'photo-vendredi-11-1664354487.png', 13, 13, '2022-09-28 10:41:27', '2022-09-28 10:41:27'),
(20, 'photo-vendredi-12-1664354491.jpg', 13, 13, '2022-09-28 10:41:31', '2022-09-28 10:41:31'),
(21, 'photo-vendredi-13-1664354496.jpg', 13, 13, '2022-09-28 10:41:36', '2022-09-28 10:41:36'),
(22, 'photo-vendredi-22-1664354503.png', 13, 13, '2022-09-28 10:41:43', '2022-09-28 10:41:43'),
(23, 'photo-vendredi-23-1664354508.png', 13, 13, '2022-09-28 10:41:48', '2022-09-28 10:41:48'),
(24, 'photo-vendredi-24-1664354513.jpg', 13, 13, '2022-09-28 10:41:53', '2022-09-28 10:41:53'),
(25, 'photo-vendredi-26-1664354519.png', 13, 13, '2022-09-28 10:41:59', '2022-09-28 10:41:59'),
(26, 'photo-vendredi-27-1664354524.png', 13, 13, '2022-09-28 10:42:04', '2022-09-28 10:42:04'),
(27, 'photo-vendredi-28-1664354529.png', 13, 13, '2022-09-28 10:42:09', '2022-09-28 10:42:09');

-- --------------------------------------------------------

--
-- Structure de la table `gallery_user`
--

CREATE TABLE `gallery_user` (
  `gallery_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `messenger_messages`
--

CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE `news` (
  `id` int NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `updated_by_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`id`, `genre`, `title`, `image`, `description`, `created_at`, `updated_at`, `created_by_id`, `updated_by_id`) VALUES
(3, 'Rock', 'Je suis un titre', 'pokemon-waves-pikachu-surfing-hd-wallpaper-1662042820.jpg', 'Je suis une description', '2022-09-01 16:33:40', '2022-09-01 16:33:40', NULL, NULL),
(4, 'Rock', 'Je suis un titre', 'towelie-300x169-1662108400.jpg', 'Miam', '2022-09-02 10:46:40', '2022-09-02 10:46:40', NULL, NULL),
(7, 'Rock', 'Je suis un titre', '4814e373a851d9e14f3fcb5699a48b1b-navy-blue-blue-and-1662109972.jpg', 'cheveux', '2022-09-02 11:12:52', '2022-09-02 11:12:52', 13, 13),
(8, 'Rock', 'Je suis un titre', 'photo-vendredi-28-1664288508.png', 'Hello', '2022-09-27 16:21:48', '2022-09-27 16:21:48', 13, 13),
(9, 'Rock', 'Je suis un titre', 'band2-bw-1664288523.png', 'hello', '2022-09-27 16:22:03', '2022-09-27 16:22:03', 13, 13);

-- --------------------------------------------------------

--
-- Structure de la table `news_user`
--

CREATE TABLE `news_user` (
  `news_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE `order` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `send_confirm` tinyint(1) NOT NULL,
  `order_confirm` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `order_details`
--

CREATE TABLE `order_details` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `orders_id` int DEFAULT NULL,
  `price` int NOT NULL,
  `quantity` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` int NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `region`
--

INSERT INTO `region` (`id`, `slug`, `code`, `name`) VALUES
(1, '', 0, ''),
(2, 'test', 25, 'test'),
(3, 'string', 30, 'string'),
(4, 'string', 0, 'string');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `username`, `avatar`, `adress`, `region`, `firstname`, `country`, `postcode`, `lastname`, `enabled`, `is_verified`, `city`, `created_at`, `updated_at`) VALUES
(13, 'abraca_admin@example.com', '[\"ROLE_ADMIN\"]', '$2y$13$WvAes2doCZWRBYK/mw3dtuDml/Ophxvcve/DhoZ9eyEqIrRTEOfHG', 'csatterfield', 'pokemon-waves-pikachu-surfing-hd-wallpaper-1662042841.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:48', '2022-09-01 16:34:01'),
(14, 'abraca_user@example.com', '[]', '$2y$13$4q3zYF2ASattsmziaHWRsuvcqMKZfb2cgYx.c67AFQhl53Xh234fK', 'cristal92', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:49', '2022-09-01 16:31:49'),
(15, 'price34@hills.com', '[]', '$2y$13$Y7md67lu6iU7lFl1Y.Z5wepGhUwyFMfDmchVWcFDRNvn2fMswFckq', 'beaulah03', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:49', '2022-09-01 16:31:49'),
(16, 'dluettgen@yahoo.com', '[]', '$2y$13$FqHAV6S2YbR3w43RDu6WBu9RpGDvA03xJ4zlsy2QyeK7XKIbD0UCC', 'sofia.runte', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:50', '2022-09-01 16:31:50'),
(17, 'khagenes@hotmail.com', '[]', '$2y$13$rwvc6uCCZpCMCeetiuHYAOzVNnPm7Qx3WG3wLF5oEm285b3si51SC', 'jamison.wiegand', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:50', '2022-09-01 16:31:50'),
(18, 'lweber@walter.net', '[]', '$2y$13$EEwtTkNkAcOGF/FNOdstBOhZrxXyo9RRx6pW1kuEVac53iDSnx8aq', 'elton.homenick', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:51', '2022-09-01 16:31:51'),
(19, 'janick15@gmail.com', '[]', '$2y$13$O7Sc3LmIITiqjrRRzj7PPus.EpoYZIZf6gr6ZKdlVBROFZTbpYGMW', 'jaylan.stiedemann', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:52', '2022-09-01 16:31:52'),
(20, 'miller.fred@gmail.com', '[]', '$2y$13$CCgIxyqYVw/s8a35f.QdZueUpii7SbMsd7qRT1EHRCJmmBTw5BQ3S', 'nicolas.virgil', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:52', '2022-09-01 16:31:52'),
(21, 'lisette91@hotmail.com', '[]', '$2y$13$v1SHK9v16i7AndVGsgtUheeYR3IBsFUF8U0hi.Sz8AGTczQ9TFgo.', 'kwolff', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:53', '2022-09-01 16:31:53'),
(22, 'linwood11@gmail.com', '[]', '$2y$13$q7IOqEmIFGC4qA2fxLIICey5ThtdE.PgXWWWQFwfT1k.Ucar5GtpW', 'earlene.schuster', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:53', '2022-09-01 16:31:53'),
(23, 'jedediah55@carroll.com', '[]', '$2y$13$0koDJad.SVpDNydKr2fFnO4xhj8vf19yjdOlVTZksRulM8ZQm84aG', 'cara.okuneva', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:54', '2022-09-01 16:31:54'),
(24, 'millie66@homenick.com', '[]', '$2y$13$bYVr.f3KWIBlANQyiXV4hOoOCNdBJlBw9I49Y6kUalaO37L/y4kPS', 'zoe.mcdermott', 'towel.jpg', '2 rue des houlettes', 'Franche-comté', 'John', 'FR', 25000, 'Mac', 1, 0, 'Besançon', '2022-09-01 16:31:55', '2022-09-01 16:31:55');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2D5B0234AE80F5DF` (`department_id`);

--
-- Index pour la table `concert`
--
ALTER TABLE `concert`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `concert_user`
--
ALTER TABLE `concert_user`
  ADD PRIMARY KEY (`concert_id`,`user_id`),
  ADD KEY `IDX_282EE28E83C97B2E` (`concert_id`),
  ADD KEY `IDX_282EE28EA76ED395` (`user_id`);

--
-- Index pour la table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CD1DE18A98260155` (`region_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_472B783AB03A8386` (`created_by_id`),
  ADD KEY `IDX_472B783A896DBBDE` (`updated_by_id`);

--
-- Index pour la table `gallery_user`
--
ALTER TABLE `gallery_user`
  ADD PRIMARY KEY (`gallery_id`,`user_id`),
  ADD KEY `IDX_1E8CFEC54E7AF8F` (`gallery_id`),
  ADD KEY `IDX_1E8CFEC5A76ED395` (`user_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C53D045F4584665A` (`product_id`);

--
-- Index pour la table `messenger_messages`
--
ALTER TABLE `messenger_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  ADD KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  ADD KEY `IDX_75EA56E016BA31DB` (`delivered_at`);

--
-- Index pour la table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD39950B03A8386` (`created_by_id`),
  ADD KEY `IDX_1DD39950896DBBDE` (`updated_by_id`);

--
-- Index pour la table `news_user`
--
ALTER TABLE `news_user`
  ADD PRIMARY KEY (`news_id`,`user_id`),
  ADD KEY `IDX_584E20C2B5A459A0` (`news_id`),
  ADD KEY `IDX_584E20C2A76ED395` (`user_id`);

--
-- Index pour la table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F5299398A76ED395` (`user_id`);

--
-- Index pour la table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_845CA2C14584665A` (`product_id`),
  ADD KEY `IDX_845CA2C1CFFE9AD6` (`orders_id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `city`
--
ALTER TABLE `city`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `concert`
--
ALTER TABLE `concert`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `department`
--
ALTER TABLE `department`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `messenger_messages`
--
ALTER TABLE `messenger_messages`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `news`
--
ALTER TABLE `news`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `order`
--
ALTER TABLE `order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `FK_2D5B0234AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

--
-- Contraintes pour la table `concert_user`
--
ALTER TABLE `concert_user`
  ADD CONSTRAINT `FK_282EE28E83C97B2E` FOREIGN KEY (`concert_id`) REFERENCES `concert` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_282EE28EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `FK_CD1DE18A98260155` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Contraintes pour la table `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `FK_472B783A896DBBDE` FOREIGN KEY (`updated_by_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_472B783AB03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `gallery_user`
--
ALTER TABLE `gallery_user`
  ADD CONSTRAINT `FK_1E8CFEC54E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1E8CFEC5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045F4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD39950896DBBDE` FOREIGN KEY (`updated_by_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_1DD39950B03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `news_user`
--
ALTER TABLE `news_user`
  ADD CONSTRAINT `FK_584E20C2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_584E20C2B5A459A0` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK_845CA2C14584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_845CA2C1CFFE9AD6` FOREIGN KEY (`orders_id`) REFERENCES `order` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
