<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class VideoController extends AbstractController
{
 #[Route('/video', name: 'app_video')]
    public function showVideo(): Response
    {
        return $this->render('video/videopage.html.twig', [
            'controller_name' => 'VideoController',
        ]);
    }
}