<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MusicController extends AbstractController
{
    #[Route('/music', name: 'app_music')]
    public function showMusic(): Response
    {
        return $this->render('music/musicpage.html.twig', [
            'controller_name' => 'MusicController',
        ]);
    }
}